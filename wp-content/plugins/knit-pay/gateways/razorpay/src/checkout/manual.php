<input id="rzp-button1" class="pronamic-pay-btn" type="submit" name="pay" value="Pay" />
<script src="https://checkout.razorpay.com/v1/checkout.js"></script>
<script>
// Checkout details as a json
var options = <?php echo $data_json; ?>;

// Boolean whether to show image inside a white frame. (default: true)
options.theme.image_padding = false;

options.modal = {
	ondismiss: function() {
		window.location.href = options.callback_url + '&action=cancelled';
	},
	// Boolean indicating whether pressing escape key 
	// should close the checkout form. (default: true)
	escape: true,
	// Boolean indicating whether clicking translucent blank
	// space outside checkout form should close the form. (default: false)
	backdropclose: false
};

var rzp = new Razorpay(options);

document.getElementById('rzp-button1').onclick = function(e){
	rzp.open();
	e.preventDefault();
}
</script>
