<?php

namespace KnitPay\Gateways\Stripe;

use Pronamic\WordPress\Pay\AbstractGatewayIntegration;
use Pronamic\WordPress\Pay\Payments\Payment;

/**
 * Title: Stripe Integration
 * Copyright: 2020-2021 Knit Pay
 *
 * @author  Knit Pay
 * @version 1.0.0
 * @since   3.1.0
 */
class Integration extends AbstractGatewayIntegration {
	/**
	 * Construct Stripe integration.
	 *
	 * @param array $args Arguments.
	 */
	public function __construct( $args = array() ) {
		$args = wp_parse_args(
			$args,
			array(
				'id'          => 'stripe',
				'name'        => 'Stripe',
				'product_url' => 'http://go.thearrangers.xyz/stripe?utm_source=knit-pay&utm_medium=ecommerce-module&utm_campaign=module-admin&utm_content=product_url',
				'provider'    => 'stripe',
			)
		);

		parent::__construct( $args );
	}

	/**
	 * Get settings fields.
	 *
	 * @return array
	 */
	public function get_settings_fields() {
		$fields = array();

		// Publishable Key.
		$fields[] = array(
			'section'  => 'general',
			'filter'   => FILTER_SANITIZE_STRING,
			'meta_key' => '_pronamic_gateway_stripe_publishable_key',
			'title'    => __( 'Publishable Key', 'knit-pay' ),
			'type'     => 'text',
			'classes'  => array( 'regular-text', 'code' ),
			'tooltip'  => __( 'This is the identifier for your Stripe merchant Account.', 'knit-pay' ),
		);

		// Secret Key.
		$fields[] = array(
			'section'  => 'general',
			'filter'   => FILTER_SANITIZE_STRING,
			'meta_key' => '_pronamic_gateway_stripe_secret_key',
			'title'    => __( 'Secret Key', 'knit-pay' ),
			'type'     => 'text',
			'classes'  => array( 'regular-text', 'code' ),
			'tooltip'  => __( 'This is the access code for your application.', 'knit-pay' ),
		);

		// Test Publishable Key.
		$fields[] = array(
			'section'  => 'general',
			'filter'   => FILTER_SANITIZE_STRING,
			'meta_key' => '_pronamic_gateway_stripe_test_publishable_key',
			'title'    => __( 'Publishable Key (Test)', 'knit-pay' ),
			'type'     => 'text',
			'classes'  => array( 'regular-text', 'code' ),
			'tooltip'  => __( 'This is the identifier for your Stripe merchant Account.', 'knit-pay' ),
		);

		// Test Secret Key.
		$fields[] = array(
			'section'  => 'general',
			'filter'   => FILTER_SANITIZE_STRING,
			'meta_key' => '_pronamic_gateway_stripe_test_secret_key',
			'title'    => __( 'Secret Key (Test)', 'knit-pay' ),
			'type'     => 'text',
			'classes'  => array( 'regular-text', 'code' ),
			'tooltip'  => __( 'This is the access code for your application.', 'knit-pay' ),
		);

		// Payment Currency.
		$fields[] = array(
			'section'  => 'advanced',
			'filter'   => FILTER_SANITIZE_STRING,
			'meta_key' => '_pronamic_gateway_stripe_payment_currency',
			'title'    => __( 'Payment Currency', 'knit-pay' ),
			'type'     => 'text',
			'classes'  => array( 'regular-text', 'code' ),
			'tooltip'  => __( '3 Character Currency Code. (eg. USD) ', 'knit-pay' ),
		);

		// Exchange Rate.
		$fields[] = array(
			'section'  => 'advanced',
			'filter'   => FILTER_VALIDATE_FLOAT,
			'meta_key' => '_pronamic_gateway_stripe_exchange_rate',
			'title'    => __( 'Exchange Rate', 'knit-pay' ),
			'type'     => 'text',
			'classes'  => array( 'regular-text', 'code' ),
			'tooltip'  => __( 'Exchange Rate of Payment Currency.', 'knit-pay' ),
			'default'  => 1,
		);

		// Return fields.
		return $fields;
	}

	/**
	 * Get configuration by post ID.
	 *
	 * @param int $post_id Post ID.
	 * @return Config
	 */
	public function get_config( $post_id ) {
		$config = new Config();

		$config->publishable_key      = $this->get_meta( $post_id, 'stripe_publishable_key' );
		$config->secret_key           = $this->get_meta( $post_id, 'stripe_secret_key' );
		$config->test_publishable_key = $this->get_meta( $post_id, 'stripe_test_publishable_key' );
		$config->test_secret_key      = $this->get_meta( $post_id, 'stripe_test_secret_key' );
		$config->payment_currency     = $this->get_meta( $post_id, 'stripe_payment_currency' );
		$config->exchange_rate        = $this->get_meta( $post_id, 'stripe_exchange_rate' );
		$config->mode                 = $this->get_meta( $post_id, 'mode' );

		return $config;
	}

	/**
	 * Get gateway.
	 *
	 * @param int $config_id Post ID.
	 * @return Gateway
	 */
	public function get_gateway( $config_id ) {
		return new Gateway( $this->get_config( $config_id ) );
	}
}
