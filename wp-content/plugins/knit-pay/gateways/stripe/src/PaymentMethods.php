<?php

namespace KnitPay\Gateways\Stripe;

use Pronamic\WordPress\Pay\Core\PaymentMethods as Core_PaymentMethods;

class PaymentMethods extends Core_PaymentMethods {
	const STRIPE = 'stripe';
}
