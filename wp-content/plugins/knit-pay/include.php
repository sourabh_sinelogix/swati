<?php

// Cashfree
require_once plugin_dir_path( __FILE__ ) . 'gateways/cashfree/src/Integration.php';
require_once plugin_dir_path( __FILE__ ) . 'gateways/cashfree/src/Gateway.php';
require_once plugin_dir_path( __FILE__ ) . 'gateways/cashfree/src/Config.php';
require_once plugin_dir_path( __FILE__ ) . 'gateways/cashfree/src/PaymentMethods.php';
require_once plugin_dir_path( __FILE__ ) . 'gateways/cashfree/src/Statuses.php';
require_once plugin_dir_path( __FILE__ ) . 'gateways/cashfree/src/Listener.php';

// CCAvenue
require_once plugin_dir_path( __FILE__ ) . 'gateways/ccavenue/src/Integration.php';
require_once plugin_dir_path( __FILE__ ) . 'gateways/ccavenue/src/Gateway.php';
require_once plugin_dir_path( __FILE__ ) . 'gateways/ccavenue/src/Config.php';
require_once plugin_dir_path( __FILE__ ) . 'gateways/ccavenue/src/PaymentMethods.php';
require_once plugin_dir_path( __FILE__ ) . 'gateways/ccavenue/src/Statuses.php';

// EBS
require_once plugin_dir_path( __FILE__ ) . 'gateways/ebs/src/Integration.php';
require_once plugin_dir_path( __FILE__ ) . 'gateways/ebs/src/Gateway.php';
require_once plugin_dir_path( __FILE__ ) . 'gateways/ebs/src/Config.php';
require_once plugin_dir_path( __FILE__ ) . 'gateways/ebs/src/PaymentMethods.php';
require_once plugin_dir_path( __FILE__ ) . 'gateways/ebs/src/Statuses.php';

// Instamojo
require_once plugin_dir_path( __FILE__ ) . 'gateways/instamojo/src/Integration.php';
require_once plugin_dir_path( __FILE__ ) . 'gateways/instamojo/src/Gateway.php';
require_once plugin_dir_path( __FILE__ ) . 'gateways/instamojo/src/Config.php';
require_once plugin_dir_path( __FILE__ ) . 'gateways/instamojo/src/PaymentMethods.php';
require_once plugin_dir_path( __FILE__ ) . 'gateways/instamojo/src/Statuses.php';

// EaseBuzz
require_once plugin_dir_path( __FILE__ ) . 'gateways/easebuzz/src/Integration.php';
require_once plugin_dir_path( __FILE__ ) . 'gateways/easebuzz/src/Gateway.php';
require_once plugin_dir_path( __FILE__ ) . 'gateways/easebuzz/src/Config.php';
require_once plugin_dir_path( __FILE__ ) . 'gateways/easebuzz/src/PaymentMethods.php';
require_once plugin_dir_path( __FILE__ ) . 'gateways/easebuzz/src/Statuses.php';
require_once plugin_dir_path( __FILE__ ) . 'gateways/easebuzz/src/Listener.php';

// PayUMoney
require_once plugin_dir_path( __FILE__ ) . 'gateways/payumoney/src/Integration.php';
require_once plugin_dir_path( __FILE__ ) . 'gateways/payumoney/src/Gateway.php';
require_once plugin_dir_path( __FILE__ ) . 'gateways/payumoney/src/Config.php';
require_once plugin_dir_path( __FILE__ ) . 'gateways/payumoney/src/PaymentMethods.php';
require_once plugin_dir_path( __FILE__ ) . 'gateways/payumoney/src/Client.php';
require_once plugin_dir_path( __FILE__ ) . 'gateways/payumoney/src/Statuses.php';
require_once plugin_dir_path( __FILE__ ) . 'gateways/payumoney/src/Listener.php';

// Razorpay
require_once plugin_dir_path( __FILE__ ) . 'gateways/razorpay/src/Integration.php';
require_once plugin_dir_path( __FILE__ ) . 'gateways/razorpay/src/Gateway.php';
require_once plugin_dir_path( __FILE__ ) . 'gateways/razorpay/src/Config.php';
require_once plugin_dir_path( __FILE__ ) . 'gateways/razorpay/src/PaymentMethods.php';
require_once plugin_dir_path( __FILE__ ) . 'gateways/razorpay/src/Statuses.php';
require_once plugin_dir_path( __FILE__ ) . 'gateways/razorpay/src/Listener.php';
require_once plugin_dir_path( __FILE__ ) . 'gateways/razorpay/src/Subs.php';

// Sodexo
require_once plugin_dir_path( __FILE__ ) . 'gateways/sodexo/src/Integration.php';
require_once plugin_dir_path( __FILE__ ) . 'gateways/sodexo/src/Gateway.php';
require_once plugin_dir_path( __FILE__ ) . 'gateways/sodexo/src/Config.php';
require_once plugin_dir_path( __FILE__ ) . 'gateways/sodexo/src/PaymentMethods.php';
require_once plugin_dir_path( __FILE__ ) . 'gateways/sodexo/src/Statuses.php';

// Stripe
require_once plugin_dir_path( __FILE__ ) . 'gateways/stripe/src/Integration.php';
require_once plugin_dir_path( __FILE__ ) . 'gateways/stripe/src/Gateway.php';
require_once plugin_dir_path( __FILE__ ) . 'gateways/stripe/src/Config.php';
require_once plugin_dir_path( __FILE__ ) . 'gateways/stripe/src/PaymentMethods.php';
require_once plugin_dir_path( __FILE__ ) . 'gateways/stripe/src/Statuses.php';
// Stripe Connect
require_once plugin_dir_path( __FILE__ ) . 'gateways/stripe-connect/src/Integration.php';
require_once plugin_dir_path( __FILE__ ) . 'gateways/stripe-connect/src/Gateway.php';
require_once plugin_dir_path( __FILE__ ) . 'gateways/stripe-connect/src/Config.php';

// Test Gateway.
require_once plugin_dir_path( __FILE__ ) . 'gateways/test/src/Integration.php';
require_once plugin_dir_path( __FILE__ ) . 'gateways/test/src/Gateway.php';
require_once plugin_dir_path( __FILE__ ) . 'gateways/test/src/Config.php';

// AWP Classifieds
require_once plugin_dir_path( __FILE__ ) . 'extensions/awp-classifieds/src/Extension.php';
require_once plugin_dir_path( __FILE__ ) . 'extensions/awp-classifieds/src/AWPCPDependency.php';

// Bookly Pro
require_once plugin_dir_path( __FILE__ ) . 'extensions/bookly-pro/src/Extension.php';
require_once plugin_dir_path( __FILE__ ) . 'extensions/bookly-pro/src/BooklyProDependency.php';

// Events Manager Pro
require_once plugin_dir_path( __FILE__ ) . 'extensions/events-manager-pro/src/Extension.php';
require_once plugin_dir_path( __FILE__ ) . 'extensions/events-manager-pro/src/EventsManagerProDependency.php';

// LearnDash
require_once plugin_dir_path( __FILE__ ) . 'extensions/learndash/src/Extension.php';
require_once plugin_dir_path( __FILE__ ) . 'extensions/learndash/src/LearnDashDependency.php';

// LearnPress
require_once plugin_dir_path( __FILE__ ) . 'extensions/learnpress/src/Extension.php';
require_once plugin_dir_path( __FILE__ ) . 'extensions/learnpress/src/LearnPressDependency.php';

// LifterLMS
require_once plugin_dir_path( __FILE__ ) . 'extensions/lifterlms/src/Extension.php';
require_once plugin_dir_path( __FILE__ ) . 'extensions/lifterlms/src/LifterLMSDependency.php';

// MotoPress Hotel Booking.
require_once plugin_dir_path( __FILE__ ) . 'extensions/motopress-hotel-booking/src/Extension.php';
require_once plugin_dir_path( __FILE__ ) . 'extensions/motopress-hotel-booking/src/MotoPressHotelBookingDependency.php';

// myCRED - buyCRED.
require_once plugin_dir_path( __FILE__ ) . 'extensions/mycred-buycred/src/Extension.php';
require_once plugin_dir_path( __FILE__ ) . 'extensions/mycred-buycred/src/MyCredDependency.php';

// Paid Memberships Pro
require_once plugin_dir_path( __FILE__ ) . 'extensions/paid-memberships-pro/src/Extension.php';
require_once plugin_dir_path( __FILE__ ) . 'extensions/paid-memberships-pro/src/PaidMembershipsProDependency.php';

// RestroPress
require_once plugin_dir_path( __FILE__ ) . 'extensions/restropress/src/Extension.php';
require_once plugin_dir_path( __FILE__ ) . 'extensions/restropress/src/RestroPressDependency.php';

// Tour Master
 require_once plugin_dir_path( __FILE__ ) . 'extensions/tourmaster/src/Extension.php';
require_once plugin_dir_path( __FILE__ ) . 'extensions/tourmaster/src/TourMasterDependency.php';

// WP Travel Engine
require_once plugin_dir_path( __FILE__ ) . 'extensions/wp-travel-engine/src/Extension.php';
require_once plugin_dir_path( __FILE__ ) . 'extensions/wp-travel-engine/src/WPTravelEngineDependency.php';

// Add Knit Pay Deactivate Confirmation Box on Plugin Page
require_once 'includes/plugin-deactivate-confirmation.php';

if ( ! function_exists( 'ppp' ) ) {
	function ppp( $a = '' ) {
		print_r( $a );
	}
}

if ( ! function_exists( 'ddd' ) ) {
	function ddd( $a = '' ) {
		echo nl2br( $a . "\r\n\n\n\n\n\n\n\n\n" );
		debug_print_backtrace();
		die( $a );
	}
}
